import React, { Component } from 'react';
import './App.css';
import 'semantic-ui-css/semantic.min.css';
import { Route } from 'react-router-dom';
import { Container } from 'semantic-ui-react';
import moviesPage from './components/pages/moviesPage';
import newMoviePage from './components/pages/newMoviePage';
import homePage from './components/pages/homePage';
import Footer from './components/Footer';
import Header from './components/Header';

class App extends Component {
  render() {
    return (
        <div className="App">
          <div>
            <Header/>
            <Container text>
              <Route exact path='/' component={homePage}></Route>
              <Route exact path='/movies' component={moviesPage}></Route>
              {/*<Route exact path='/movies/new' component={newMoviePage}></Route>*/}
              <Route exact path='/movies/:_id' component={newMoviePage}></Route>
            </Container>
            <Footer/>
          </div>
        </div>
    );
  }
}

export default App;
