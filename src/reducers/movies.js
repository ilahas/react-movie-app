import {
    FETCHED_MOVIES_PENDING,
    FETCHED_MOVIES_FULFILLED,
    FETCHED_MOVIES_REJECTED,

    DELETE_MOVIE_PENDING,
    DELETE_MOVIE_FULFILLED,
    DELETE_MOVIE_REJECTED
} from "../actions/movies";

const initialState = {
    moviesList: [],
    fetching: false,
    error: {}
}
export default (state = initialState, action) => {
    switch (action.type) {
        case FETCHED_MOVIES_PENDING:
            return {
                ...state,
                fetching:true
            }
        case FETCHED_MOVIES_FULFILLED:
            return {
                ...state,
                moviesList:action.payload,
                fetching:false
            }
        case FETCHED_MOVIES_REJECTED:
            return {
                ...state,
                error:action.payload,
                fetching:false
            }

            // delete
        case DELETE_MOVIE_PENDING:
            return {
                ...state
            }
        case DELETE_MOVIE_FULFILLED:
            return {
                ...state,
                moviesList:state.moviesList.filter(item => item._id !== action.payload.id)
            }
        case DELETE_MOVIE_REJECTED:
            return {
                ...state,
                error:action.payload
            }
        default:
            return state;
    }
}