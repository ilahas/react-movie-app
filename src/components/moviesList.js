import React from 'react';
import PropTypes from 'prop-types';
import MovieCard from './movieCard';
import { Grid } from 'semantic-ui-react';
import { ClipLoader } from "react-spinners";

const MoviesList = ({movies, onDelete}) => {

    const emptyMessage = (
        <p>There are not any movies yet.</p>
    );

    const moviesList = (
        <div>
            <ClipLoader
                size={60}
                color={"#123abc"}
                loading={movies.fetching}
            />

            {
                movies.error.response
                    ? <p>Error retrieving data</p>
                    :
                    <Grid stackable columns={3}>
                        {
                            movies.moviesList.map(movie =>
                                <MovieCard key={movie._id} movie={movie} onDelete={onDelete}/>)
                        }
                    </Grid>

            }
        </div>
    );


    return (
        <div>
            {movies.length === 0 ? emptyMessage : moviesList}
        </div>
    );
};

MoviesList.propTypes = {
    movies: PropTypes.shape({
        moviesList: PropTypes.array.isRequired
    }).isRequired
};

export default MoviesList;