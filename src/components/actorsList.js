import React from 'react';
import {Card, Grid} from "semantic-ui-react";

const ActorsList = props => {
    return (
        <div>
            <Grid stackable columns={3}>
                {
                    props.actors.map((actor,key) =>
                        <Grid.Column key = {key}>
                            <Card>
                                <Card
                                    image={actor.photo}
                                    header={ actor.name}
                                    extra={actor.description}
                                />
                            </Card>
                        </Grid.Column>
                    )
                }
            </Grid>
        </div>
    );
}


ActorsList.propTypes = {};

export default ActorsList;