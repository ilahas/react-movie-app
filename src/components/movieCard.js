import React from 'react'
import { Card, Grid, Button, Icon } from 'semantic-ui-react'
import {Link} from 'react-router-dom';
import {onDeleteMovieSubmit} from "../actions/movies";
import {style} from "redux-logger/src/diff";



const extra = (movie, onDelete) => {
    return (
        <div className="ui two buttons">
            <Button animated as={Link} to={`/movies/${movie._id}`}>
                <Button.Content visible>Edit</Button.Content>
                <Button.Content hidden>
                    <Icon name='arrow right'/>
                </Button.Content>
            </Button>
            <Button animated='vertical' onClick={()=>onDelete(movie._id)}>
                <Button.Content hidden>Delete</Button.Content>
                <Button.Content visible>
                    <Icon name='trash' />
                </Button.Content>
            </Button>
        </div>
    )
};

const MovieCard = ({movie, onDelete}) => (
    <Grid.Column>
        <Card>
            <Card
                image={movie.cover}
                header={ movie.title}
                extra={extra(movie, onDelete)}
            />
        </Card>
    </Grid.Column>
)

export default MovieCard