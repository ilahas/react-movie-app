import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import MoviesList from '../moviesList';
import {
    fetchMovies,
    onDeleteMovieSubmit
} from "../../actions/movies";


class MoviesPage extends Component {

    static propTypes = {
        movies: PropTypes.object.isRequired
    };

    componentDidMount() {
        this.props.fetchMovies();
    }


    render() {
        return (
            <div>
                <h2>Movies Page</h2>
                <MoviesList
                    movies={this.props.movies}
                    onDelete = {this.props.onDeleteMovieSubmit}
                />
            </div>
        );
    }
}

MoviesPage.propTypes = {};

const mapStateToProps = ({movies}) => {
    return {
        movies
    }
};

const mapDispatchToProps =  {
        fetchMovies,
        onDeleteMovieSubmit
}

export default connect(mapStateToProps, mapDispatchToProps)(MoviesPage);