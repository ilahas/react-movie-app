import React, {Component} from 'react';
import PropTypes from 'prop-types';
import MovieCard from "../moviesList";
import {Grid} from "semantic-ui-react";
import ActorsList from '../actorsList';

class HomePage extends Component {
    state = {
        actors: [{
            name: 'Leonardo Dicaprio',
            photo: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTl1jNqpKtX7TgqlptDgVVbvWKeuKJO_IyMW8CgS99yWGf9jPMmQg&s',
            description: 'The best actor of me'
        }]
    }

    render() {
        return (
            <div>
                <ActorsList actors={this.state.actors}></ActorsList>
            </div>
        );
    }
}

HomePage.propTypes = {};

export default HomePage;